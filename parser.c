#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
#include <malloc.h>
#include <assert.h>

#include "builtins.h"
#include "runtime.h"

#define INIT 0
#define WORD 1
#define COMM_A 2
#define COMM_B 3

#define WORD_MAX_CAP 32
#define MACRO_START '{'
#define MACRO_END '}'
#define MACRO_MAX_INST 128

/* Unresolved macro list */
struct {
        uint64_t len;
        uint64_t cap;
        struct inst_t **list;
        char **names;
} unres_macros;

int32_t read_word(FILE *f, char *buf)
{
        int32_t c;
        int32_t i = 0;
        bool end = false;
        int16_t state = INIT;
        
        while (!end) {
                c = fgetc(f);
                switch (state) {
                case INIT:
                        if (c == EOF) {
                                end = true;
                        } else if (c == MACRO_START ||
                            c == MACRO_END) {
                                buf[i++] = c;
                                end = true;
                        } else if (c == ' ' ||
                                   c == '\n' ||
                                   c == '\t') {
                                state = INIT;
                        } else if (c == ',') {
                                state = COMM_A;
                        } else if (isgraph(c)) {
                                state = WORD;
                                buf[i++] = toupper(c);
                        } else {
                                fprintf(stderr, "unexpected character found with code: 0x%x\n", c);
                                return -1;
                        }
                        break;
                case WORD:
                        if (c == ' '  ||
                            c == '\n' ||
                            c == '\t' ||
                            c == EOF) {
                                end = true;
                        } else if (c == ',') {
                                state = COMM_B;
                        } else if (isgraph(c)) {
                                buf[i++] = toupper(c);
                        } else {
                                fprintf(stderr, "unexpected character found with code: 0x%x\n", c);
                                return -1;
                        }
                        break;
                case COMM_A:
                        if (c == EOF) {
                                end = true;
                        } else if (c == '\n') {
                                state = INIT;
                        }
                        break;
                case COMM_B:
                        if (c == EOF || c == '\n') {
                                end = true;
                        }
                        break;
                }
        }
        buf[i++] = 0;
        return i-1;
}

void fill_inst_call(const char *buf, struct inst_t *inst)
{
        char *name;
        uint64_t i;
        for (i = 0; i < BUILTINS_MAP_SIZE; ++i) {
                if (strcmp(buf, builtins_map[i].name) == 0) {
                        printf(" \t[B]\n");
                        inst->type = BUILTIN;
                        inst->ptr.b = &builtins_map[i];
                        /* printf("  %s\n", inst->ptr.b->name); */
                        return;
                }
        }
        printf(" \t[M]\n");
        inst->type = MACRO;

        name = malloc(sizeof(char *) * WORD_MAX_CAP);
        memcpy(name, buf, WORD_MAX_CAP);
        unres_macros.names[unres_macros.len] = name;
        
        unres_macros.list[unres_macros.len++] = inst;
}

int32_t parse_macro_def(FILE *f, struct macro_t *m)
{
        char buf[WORD_MAX_CAP];
        char *inst;
        int32_t str_len;
        uint32_t i = 0;

        str_len = read_word(f, buf);
        if (str_len == -1) { // ERROR
                fprintf(stderr, "error reading macro name\n");
                return -1;
        }
        if (str_len == 0) { // EOF
                fprintf(stderr, "unexpected EOF reading macro name\n");
                return -1;                        
        }
        m->name = malloc(sizeof(*m->name) * str_len);
        memcpy(m->name, buf, str_len);
        printf("macro: `%s' {\n", m->name);

        m->body = malloc(sizeof(*m->body) * MACRO_MAX_INST);
        while (true) {
                str_len = read_word(f, buf);
                if (str_len == -1) { // ERROR
                        fprintf(stderr, "error reading macro `%s' definition\n", m->name);
                        return -1;
                }
                if (str_len == 0) { // EOF
                        fprintf(stderr, "unexpected EOF reading macro `%s' definition\n", m->name);
                        return -1;                        
                }
                if (buf[0] == MACRO_END) {
                        printf("}\n");
                        break;
                }

                printf("  `%s' ", buf);
                fill_inst_call(buf, m->body+i);
                i++;
        }
        
        m->len = i;
        return 0;
}

void free_macro(struct macro_t *m)
{
        free(m->name);
        free(m->body);
}

int32_t parse_inst(FILE *f)
{
        char buf[WORD_MAX_CAP];
        int32_t str_len;

        struct macro_t *cur_macro = macro_map.list + macro_map.len;
        struct inst_t *cur_inst = inst_list.list + inst_list.len;

        assert(macro_map.len < macro_map.cap);
        assert(inst_list.len < inst_list.cap);

        str_len = read_word(f, buf);
        if (!str_len) {
                return -2;
        }
        if (buf[0] == MACRO_START) {
                str_len = parse_macro_def(f, cur_macro);
                ++macro_map.len;
        } else {
                printf("inst: `%s' ", buf);
                fill_inst_call(buf, cur_inst);
                ++inst_list.len;
        }
        return str_len;
}

void print_macro(const struct macro_t *m)
{
        printf("macro: {\n");
        printf("name: `%s'\nbody:\n", m->name);
        printf("}\n");
}

int32_t fill_macro_call(const char *buf, struct inst_t *m)
{
        uint64_t i;
        printf("fill macro call `%s'", buf);
        for (i = 0; i < macro_map.len; ++i) {
                if (strcmp(macro_map.list[i].name, buf) == 0) {
                        m->ptr.m = macro_map.list+i;
                        printf("  %s\n", m->ptr.m->name);
                        return 0;
                }
        }
        return -1; // Macro could not be found
}

int32_t resolve_macros(void)
{
        uint64_t i;
        struct inst_t *inst;
        char *name;
        
        for (i = 0; i < unres_macros.len; ++i) {
                inst = unres_macros.list[i];
                name = unres_macros.names[i];
                if (fill_macro_call(name, inst) != 0) {
                        // Report error
                        fprintf(stderr, "unresolvable macro call `%s'\n", name);
                        return -1;
                }
        }
        return 0;
}

void init_inst_list(uint64_t cap)
{
        inst_list.list = malloc(sizeof(*inst_list.list) * cap);
        inst_list.cap = cap;
        inst_list.len = 0;
}

void init_unres_macros(uint64_t cap)
{
        unres_macros.list = malloc(sizeof(*unres_macros.list) * cap);
        unres_macros.names = malloc(sizeof(*unres_macros.names) * cap);
        unres_macros.cap = cap;
        unres_macros.len = 0;
}

void init_macro_map(uint64_t cap)
{
        macro_map.list = malloc(sizeof(*macro_map.list) * cap);
        macro_map.cap = cap;
        macro_map.len = 0;
}

void free_inst_list()
{
        free(inst_list.list);
}

void free_unres_macros()
{
        free(unres_macros.list);
        for (uint64_t i = 0; i < unres_macros.len; ++i) {
                free(unres_macros.names[i]);
        }
        free(unres_macros.names);
}

void free_macro_map()
{
        for (uint64_t i = 0; i < macro_map.len; ++i) {
                free_macro(&macro_map.list[i]);
        }
        free(macro_map.list);
}

int32_t main(int32_t argc, char *argv[])
{
        FILE *f;
        if (argc != 2) {
                printf("use: `%s <filename>`\n", argv[0]);
                return -1;
        }
        init_inst_list(128);
        init_unres_macros(128);
        init_macro_map(128);

        f = fopen(argv[1], "r");
        if (f == NULL) {
                perror("fopen");
                return -1;
        }

        int32_t ret;
        bool end = false;
        bool err = false;
        while (!end) {
                ret = parse_inst(f);
                switch (ret) {
                case -1:
                        err = true;
                        end = true;
                case -2:
                        end = true;
                        break;
                default:
                        break;
                }
        }
        if (!err) {
                ret = resolve_macros();
        }
        free_unres_macros();
        if (ret == 0) {
                exec_prog();
        }
        free_macro_map();
        free_inst_list();

        fclose(f);
}

