
, basic macro definitions
{ inc2
  inc inc
}

{ inc5
  inc2 inc2 inc
}

{ inc7
  inc5
  inc2
}

{ inc10
  inc5 inc5
}

{ inc50
  inc10 inc10 inc10 inc10 inc10
}

{ inc100
  inc50 inc50
}

{ inc500
  inc100 inc100 inc100 inc100 inc100
}

{ inc1000
  inc500 inc500
}

{ ?top
  dup
  ?print
}

, init two stacks with 0
{ init
  zero
  switch
  zero
  switch
}

{ mov
  switch
  zero
  switch
  swap
  pop
}

{ copy
  dup
  mov
}

, program to print 10
init

inc10
switch
inc10
inc10
inc1000
copy


?top

