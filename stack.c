#include <stdint.h>
#include <stdio.h>
#include <malloc.h>
#include "stack.h"

#define CAP 512

int32_t stack_alloc(struct stack_t *s, uint32_t init_cap)
{
        s->stack = malloc(sizeof(*s->stack) * init_cap);
        if (s->stack == NULL) {
                return -1;
        }
        s->cap = init_cap;
        s->size = 0;

        return 0;
}

void stack_free(struct stack_t *s)
{
        free(s->stack);
}

int32_t stack_push(struct stack_t *s, int64_t num)
{
        if (s->size < s->cap) {
                s->stack[s->size++] = num;
                return 0;
        }
        return -1;
}

int32_t stack_pop(struct stack_t *s, int64_t *num)
{
        if (s->size > 0) {
                *num = s->stack[--s->size];
                return 0;
        }
        return -1;
}

int64_t *stack_top(struct stack_t *s)
{
        if (s->size == 0) {
                return NULL;
        } else {
                return &s->stack[s->size-1];
        }
}

void stack_print(const struct stack_t *s)
{
        uint64_t iter = s->size;
        printf("---\n");
        while (iter > 0) {
                printf("%ld\n", s->stack[--iter]);
        }
        printf("---\n");
}

int32_t main2(void)
{
        struct stack_t s;
        int64_t res;
        stack_alloc(&s, CAP);

        printf("Hello World\n");
        stack_free(&s);
        return 0;
}
