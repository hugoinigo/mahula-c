#ifndef BUILTINS_H_
#define BUILTINS_H_

#include "stack.h"

struct state_t {
        struct stack_t stack[2];
        uint64_t cur_stack;
};

#define ARGS struct state_t *state
#define RET void *

RET zero(ARGS);
RET inc(ARGS);
RET dec(ARGS);
RET switc(ARGS);
RET dup(ARGS);
RET swap(ARGS);
RET pop(ARGS);
RET print(ARGS);

#endif // BUILTIN_H_
