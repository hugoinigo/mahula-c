#ifndef STACK_H_
#define STACK_H_

#include <stdint.h>

struct stack_t {
        uint32_t cap;
        uint64_t size;
        int64_t *stack;
};

int32_t stack_alloc(struct stack_t *s, uint32_t init_cap);
void stack_free(struct stack_t *s);

int32_t stack_push(struct stack_t *s, int64_t num);
int32_t stack_pop(struct stack_t *s, int64_t *num);
int64_t *stack_top(struct stack_t *s);

void stack_print(const struct stack_t *s);

#endif //STACK_H
