#include "builtins.h"
#include "runtime.h"

#include <stdio.h>

RET zero(ARGS)
{
        stack_push(&state->stack[state->cur_stack], 0);
        return NULL;
}

RET inc(ARGS)
{
        int64_t *num = stack_top(&state->stack[state->cur_stack]);
        (*num)++;
        return NULL;
}

RET dec(ARGS)
{
        int64_t *num = stack_top(&state->stack[state->cur_stack]);
        (*num)--;
        return NULL;
}

RET switc(ARGS)
{
        state->cur_stack = 1 - state->cur_stack;
        return NULL;
}

RET dup(ARGS)
{
        int64_t top = *stack_top(&state->stack[state->cur_stack]);
        stack_push(&state->stack[state->cur_stack], top);
        return NULL;
}

RET swap(ARGS)
{
        int64_t *a = stack_top(&state->stack[0]);
        int64_t *b = stack_top(&state->stack[1]);
        int64_t aux = *a;
        
        *a = *b;
        *b = aux;
        return NULL;
}

RET print(ARGS)
{
        int64_t num;
        stack_pop(&state->stack[state->cur_stack], &num);
        printf("s[%ld]: %ld\n", state->cur_stack, num);
        return NULL;
}

RET pop(ARGS)
{
        int64_t num;
        stack_pop(&state->stack[state->cur_stack], &num);
        return NULL;
}
