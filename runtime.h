#ifndef RUNTIME_H_
#define RUNTIME_H_

#include <stdint.h>
#include "builtins.h"

struct builtin_t {
        const char *name;
        void* (*op) (ARGS);
};

struct macro_t {
        char *name;
        uint64_t len;
        struct inst_t *body;
};

static const
struct builtin_t builtins_map[] = {
        { "ZERO", zero },
        { "INC", inc },
        { "DEC", dec },
        { "SWITCH", switc },
        { "DUP", dup },
        { "SWAP", swap },
        { "POP", pop },
        { "?PRINT", print },
};

#define BUILTINS_MAP_SIZE (sizeof(builtins_map) / sizeof(*builtins_map))

struct macro_map_t {
        uint64_t len;
        uint64_t cap;
        struct macro_t *list;
};

/* Program's instruction list */
struct inst_list_t {
        uint64_t len;
        uint64_t cap;
        struct inst_t *list;
};

struct inst_t {
        enum {
                BUILTIN,
                MACRO
        } type;

        union {
                struct macro_t *m;
                const struct builtin_t *b;
        } ptr;
};

extern struct macro_map_t macro_map;
extern struct inst_list_t inst_list;

void exec_prog();

#endif // RUNTIME_H_
