#include <stdio.h>
#include "runtime.h"
#include "stack.h"

struct state_t state;

struct macro_map_t macro_map;
struct inst_list_t inst_list;

void exec_inst(const struct inst_t *inst);
void exec_macro_call(const struct macro_t *macro);

void exec_inst(const struct inst_t *inst)
{
                switch (inst->type) {
                case BUILTIN:
                        inst->ptr.b->op(&state);
                        break;
                case MACRO:
                        exec_macro_call(inst->ptr.m);
                        break;
                }
}

void exec_macro_call(const struct macro_t *macro)
{
        uint64_t i;
        struct inst_t *inst;
        for (i = 0; i < macro->len; ++i) {
                inst = &macro->body[i];
                exec_inst(inst);
        }
}

void exec_prog()
{
        uint64_t i;
        struct inst_t *inst;
        
        stack_alloc(&state.stack[0], 512);
        stack_alloc(&state.stack[1], 512);
        state.cur_stack = 0;
        
        printf("\n Exec:\n");
        
        for (i = 0; i < inst_list.len; ++i) {
                inst = inst_list.list+i;
                printf("[%s]\n", inst->type == BUILTIN ? inst->ptr.b->name : inst->ptr.m->name);
                exec_inst(inst);
        }

        stack_print(&state.stack[0]);
        stack_print(&state.stack[1]);
        stack_free(&state.stack[0]);
        stack_free(&state.stack[1]);

}
